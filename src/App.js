import React, {useState} from 'react';
import './App.css';
import Header from './componets/Header';
import styled from '@emotion/styled'
import Formulario from './componets/Formulario';
import Resumen from './componets/Resumen';
import Resultado from './componets/Resultado';
import Spinner from './componets/Spinner';


const Contenedor = styled.div`
    max-width: 600px;
    margin: 0 auto;
`
const ContenedorFormulario = styled.div`
  background-color: #fff;
  padding: 3rem;
`
function App() {
    const [resumen, guardarResumen]= useState({
      cotizacion: 0,
      datos: {
        marca: '',
        year: '',
        plan: ''
      }

    });
    const [cargando, guardarCargando]= useState(false);
  //extraer datos
  const {datos} = resumen;
  return (
    <Contenedor>
      <Header
      titulo='Cotizador de Seguros'
      />
      <ContenedorFormulario>
          <Formulario
            guardarResumen = {guardarResumen}
            guardarCargando = {guardarCargando}
          />
          <Resumen
            datos = {datos}
          />
          {
            (cargando)?(<Spinner/>):(null)
          }
          {(!cargando)?(<Resultado
            cotizacion={resumen.cotizacion}
          />):(null)

          }

          </ContenedorFormulario>
    </Contenedor>

  );
}

export default App;

import React from 'react';
import styled from '@emotion/styled';
import {TransitionGroup, CSSTransition } from 'react-transition-group';
import PropsTypes  from 'prop-types';

const CotizacionH1 = styled.h1`
background-color: green;
color: white;
text-align: center;
margin-top: 2rem;
padding: 1rem;
`
const CotizacionP = styled.p`
background-color: rgba(127, 224, 237);
color: white;
text-align: center;
margin-top: 2rem;
padding: 1rem;
`
const ResultadoCotizacion = styled.div`
    text-align: center;
    padding: 0.5rem;
    border: 1px solid #26c6da;
    background-color: green;
    margin-top: 1rem;
    position: relative;

`

function Resultado({cotizacion}) {
    return (

        (cotizacion === 0) ? (<CotizacionP>Elija marca, año y tipo</CotizacionP>):(
            <ResultadoCotizacion>
            <TransitionGroup
                component= "p"
                classNames="resultado"
                >
                <CSSTransition
                    classNames= "resultado"
                    key={cotizacion}
                    timeout={{enter: 500, exit: 500}}
                    >
                    <CotizacionH1> El Resultado es: {cotizacion}</CotizacionH1>
                </CSSTransition>
            </TransitionGroup>
                    </ResultadoCotizacion>
        )
    );
}

Resultado.propTypes={
    cotizacion: PropsTypes.number.isRequired,
}
export default Resultado;
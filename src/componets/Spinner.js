import React from 'react';
import './spinner.css';
function Spinner(props) {
    return (
        <div>
            <div className="spinner"></div>
        </div>
    );
}

export default Spinner;